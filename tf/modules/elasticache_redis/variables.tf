variable "subnet_ids" {
  type = "list"
}
variable "security_groups" {
  default = []
}
variable "ec_replicas" {
  default = "1"
}
variable "ec_instance_type" {
  default = "cache.m3.medium"
}
variable "ec_engine" {
  default = "redis"
}
variable "ec_engine_version" {
  default = "5.0.4"
}
variable "ec_parameter_group_name" {
  default = "default.redis5.0"
}
variable "ec_port" {
  default = "6379"
}
variable "ec_retention" {
  default = "7"
  type = "string"
}
variable "backup_window" {
  default = "07:00-09:00"
  type = "string"
}
variable "vpc_id" {
  type = "string"
}
variable "ec_name" {
  type = "string"
}
variable "ec_automatic_failover_enabled" {
  default = false
}
variable "ec_dns_name" {
  type = "string"
}
variable "ec_hosted_zone_id" {
  type = "string"
}
