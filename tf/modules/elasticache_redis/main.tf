resource "aws_elasticache_subnet_group" "ec_subnet_group_redis" {
  name = "${lower(var.ec_name)}"
  subnet_ids = ["${var.subnet_ids}"]
}

resource "aws_elasticache_replication_group" "ec_cluster" {
  replication_group_id = "${lower(var.ec_name)}"
  replication_group_description = "${lower(var.ec_name)}"
  engine = "${var.ec_engine}"
  automatic_failover_enabled = "${var.ec_automatic_failover_enabled}"
  engine_version = "${var.ec_engine_version}"
  port = "${var.ec_port}"
  node_type = "${var.ec_instance_type}"
  number_cache_clusters = "${var.ec_replicas}"
  parameter_group_name = "${var.ec_parameter_group_name}"
  security_group_ids = ["${var.security_groups}"]
  subnet_group_name = "${aws_elasticache_subnet_group.ec_subnet_group_redis.id}"
  snapshot_window = "${var.backup_window}"
  snapshot_retention_limit = "${var.ec_retention}"

  tags {
    "Name" = "${var.ec_name}"
    "Environment" = "${terraform.workspace}"
    "CreatedBy" = "Mission Cloud"
  }
}

resource "aws_route53_record" "this" {
  zone_id = "${var.ec_hosted_zone_id}"
  name    = "${var.ec_dns_name}"
  type    = "CNAME"
  ttl     = "120"

  records = ["${aws_elasticache_replication_group.ec_cluster.primary_endpoint_address}"]
}
