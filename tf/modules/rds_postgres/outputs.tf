output "rds-endpoint" {
  value = "${element(split(":", aws_db_instance.rds-instance.endpoint), 0)}"
}
output "rds_id" {
  value = "${aws_db_instance.rds-instance.id}"
}
output "rds-username" {
  value = "${aws_db_instance.rds-instance.username}"
}
output "rds-parameter-group" {
  value = "${aws_db_parameter_group.rds-param-group.id}"
}
output "rds-subnet-group" {
  value = "${aws_db_subnet_group.rds-subnet-group.id}"
}
output "rds-option-group" {
  value = "${aws_db_option_group.rds-og.id}"
}
