resource "aws_s3_bucket" "s3_rds_bucket" {
  bucket = "${var.rds_bucket}"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}
