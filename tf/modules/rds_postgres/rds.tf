resource "aws_db_instance" "rds-instance" {
  allocated_storage                   = "${var.rds_storage}"
  max_allocated_storage               = "${var.max_allocated_storage}"
  storage_type                        = "${var.rds_storage_type}"
  iops                                = "${var.rds_storage_iops}"
  engine                              = "${var.rds_engine}"
  engine_version                      = "${var.rds_engine_version}"
  license_model                       = "postgresql-license"
  instance_class                      = "${var.rds_instance_type}"
  username                            = "${var.rds_user}"
  password                            = "${var.rds_password}"
  multi_az                            = "${var.rds_multi_az}"
  db_subnet_group_name                = "${aws_db_subnet_group.rds-subnet-group.name}"
  parameter_group_name                = "${aws_db_parameter_group.rds-param-group.name}"
  option_group_name                   = "${aws_db_option_group.rds-og.name}"
  vpc_security_group_ids              =["${var.rds_security_group_ids}"]
  identifier                          = "${var.rds_name}"
  backup_retention_period             = "${var.rds_retention_period}"
  backup_window                       = "${var.rds_backup_window}"
  kms_key_id                          = "${aws_kms_key.rds-key.arn}"
  maintenance_window                  = "${var.rds_maint_window}"
  apply_immediately                   = "${var.rds_apply_immediately}"
  storage_encrypted                   = "${var.rds_storage_encrypted}"
  auto_minor_version_upgrade          = "${var.rds_apply_minor_version_upgrade}"
  publicly_accessible                 = "${var.rds_publicly_accessible}"
  skip_final_snapshot                 = "${var.rds_skip_final_snapshot}"
  deletion_protection                 = "${var.rds_deletion_protection}"
  enabled_cloudwatch_logs_exports     =["${var.rds_enabled_cloudwatch_logs_exports}"]
  performance_insights_enabled        = "${var.rds_performance_insights_enabled}"
  monitoring_interval                 = "${var.rds_monitoring_interval}"
  monitoring_role_arn                 = "${aws_iam_role.rds-og-role.arn}"

  tags {
    "Name"           = "${var.rds_name}"
    "Environment"    = "${terraform.workspace}"
    "Terraform"      = "yes"
  }
}

resource "aws_db_subnet_group" "rds-subnet-group" {
  name = "${var.rds_name}-subnet-group"

  subnet_ids = ["${var.rds_subnet_ids}"]

  tags {
    Name = "${var.rds_name} subnet group"
  }
}

resource "aws_db_parameter_group" "rds-param-group" {
  name   = "${var.rds_name}-param-group"
  family = "${var.rds_engine_family}"
}

resource "aws_db_option_group" "rds-og" {
  name                     = "${var.rds_name}-option-group"
  option_group_description = "${var.rds_name} option group"
  engine_name              = "${var.rds_engine}"
  major_engine_version     = "${var.rds_engine_version_major}"
}

# Route 53 record
resource "aws_route53_record" "this" {
  zone_id = "${var.rds_hosted_zone_id}"
  name    = "${var.rds_dns_name}"
  type    = "CNAME"
  ttl     = "120"
  records = ["${aws_db_instance.rds-instance.address}"]
}
