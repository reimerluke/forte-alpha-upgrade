#  Note:  A user defined kms key must be used.  RDS will not allow the SQL Server to use the default kms aws/rds key.

resource "aws_kms_key" "rds-key" {
  description         = "${var.rds_name} kms key"
  enable_key_rotation = true
}

resource "aws_kms_alias" "rds-key-alias" {
  name          = "alias/${var.rds_name}-kms-key"
  target_key_id = "${aws_kms_key.rds-key.key_id}"
}
