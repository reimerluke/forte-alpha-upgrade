resource "aws_iam_instance_profile" "rds-og-profile" {
  name = "${var.rds_name}-option-group-profile"
  role = "${aws_iam_role.rds-og-role.name}"
}

resource "aws_iam_role" "rds-og-role" {
  name = "${var.rds_name}-option-group-role"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement":
    [{
        "Effect": "Allow",
        "Principal": {"Service":  ["rds.amazonaws.com", "monitoring.rds.amazonaws.com"]},
        "Action": "sts:AssumeRole"
    }]
}
EOF
}

resource "aws_iam_role_policy" "rds-og-policy" {
  name = "${var.rds_name}-option-group-policy"
  role = "${aws_iam_role.rds-og-role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement":
    [
        {
        "Effect": "Allow",
        "Action":
            [
                "kms:DescribeKey",
                "kms:GenerateDataKey",
                "kms:Encrypt",
                "kms:Decrypt"
            ],
        "Resource": "${aws_kms_key.rds-key.arn}"
        },
        {
        "Effect": "Allow",
        "Action":
            [
                "s3:ListBucket",
                "s3:GetBucketLocation"
            ],
        "Resource": "arn:aws:s3:::${aws_s3_bucket.s3_rds_bucket.id}"
        },
        {
        "Effect": "Allow",
        "Action":
            [
                "s3:GetObjectMetaData",
                "s3:GetObject",
                "s3:PutObject",
                "s3:ListMultipartUploadParts",
                "s3:AbortMultipartUpload"
            ],
        "Resource": "arn:aws:s3:::${aws_s3_bucket.s3_rds_bucket.id}/*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "rds_monitoring" {
  role       = "${aws_iam_role.rds-og-role.id}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}
