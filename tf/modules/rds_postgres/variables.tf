# This files any variables that can be referenced throughout the other templates
# The value the variables are set to can be found in the terraform.tfvars file

variable "rds_name" {
  default = ""
}

variable "rds_storage" {
  default = ""
}

variable "max_allocated_storage" {
  default = ""
}

variable "rds_user" {
  default = ""
}

variable "rds_password" {
  default = ""
}

variable "rds_bucket" {
  default = ""

  # bucket to store database backups
}

variable "rds_vpc_id" {
  default = ""

  # source from VPC module
}

variable "rds_subnet_ids" {
  default = []

  # example
  # ["subnet-1234abc", "subnet-xyz987", "subnet-ghi654"]
  # source from VPC module
}

variable "rds_security_group_ids" {
  default = []

  #["sg-1234abc", "sg-xyz987", "sg-ghi654"]
  # SGs that have access to this RDS.  for example, web or openvpn SGs
}

variable "rds_security_group_cider_blocks" {
  default = []

  # IPs that have access to this RDS.
}

variable "rds_publicly_accessible" {
  default = "false"
}

variable "rds_engine" {
  default = "postgres"
}

variable "rds_engine_version" {
  default = "11.4"

  # default is Sql Server 2016 SP 1 - released Jan 2018
}

variable "rds_engine_version_major" {
  default = "11"

}

variable "rds_engine_family" {
  default = "postgres11"

  # run this to see them all aws rds describe-db-engine-versions --query "DBEngineVersions[].DBParameterGroupFamily"
}

variable "rds_instance_type" {
  default = "db.r5.large"

}

variable "rds_multi_az" {
  default = "false"

  # Can't set this for express versions
}

variable "rds_storage_type" {
  default = "gp2"
}

variable "rds_storage_iops" {
  default = ""
}

variable "rds_retention_period" {
  default = "14"
}

variable "rds_backup_window" {
  default = "07:00-07:30"

  # midnight PST
}

variable "rds_maint_window" {
  default = "Sat:08:00-Sat:08:30"

  # Saturday 1 AM PST
}

variable "rds_storage_encrypted" {
  default = "true"

  # encrypt by default.
}

variable "rds_apply_minor_version_upgrade" {
  default = "false"
}

variable "rds_skip_final_snapshot" {
  default = "true"

  # If you need to make changes to the instance that require a rebuild
  # and you want to retain the endpoint, do the following:
  # 1. Manually make a final snapshot with AWS Console *** MUST DO THIS ****
  # 2. Set this to true and apply.
  # 3. Make the changes that require rebuild and apply
}

variable "rds_apply_immediately" {
  default = "true"
}

variable "rds_deletion_protection" {
  default = "true"
}

variable "rds_enabled_cloudwatch_logs_exports" {
  default = ["postgresql", "upgrade"]
  type = "list"
}

variable "rds_monitoring_interval" {
   default = "0"
}

variable "rds_performance_insights_enabled" {
  default = "false"
}

variable "rds_dns_name" {
  type = "string"
}

variable "rds_hosted_zone_id" {
  type = "string"
}
