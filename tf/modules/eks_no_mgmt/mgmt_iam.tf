resource "aws_iam_role" "mgmt" {
  count = "${var.create_management_instance == "true" ? 1 : 0}"

  name = "${var.cluster_name}-mgmt"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": [
                 "ec2.amazonaws.com",
                 "eks.amazonaws.com"
               ]
            },
            "Effect": "Allow"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "mgmt" {
  count = "${var.create_management_instance == "true" ? 1 : 0}"

  name = "${var.cluster_name}-mgmt"
  role = "${aws_iam_role.mgmt.name}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:*",
      "Resource": [
        "${aws_s3_bucket.mgmt.arn}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "eks:*",
        "sts:AssumeRole"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "eks_cluster" {
  count = "${var.create_management_instance == "true" ? 1 : 0}"

  role       = "${aws_iam_role.mgmt.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}
resource "aws_iam_role_policy_attachment" "eks_service" {
  count = "${var.create_management_instance == "true" ? 1 : 0}"
  
  role       = "${aws_iam_role.mgmt.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
}

resource "aws_iam_instance_profile" "mgmt" {
  count = "${var.create_management_instance == "true" ? 1 : 0}"
  name = "${var.cluster_name}-mgmt"
  role = "${aws_iam_role.mgmt.name}"
}
