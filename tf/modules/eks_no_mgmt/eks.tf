module "eks" {

  source  = "terraform-aws-modules/eks/aws"
  version = "4.0.2"

  cluster_name                                 = "${var.cluster_name}"
  cluster_version                              = "${var.cluster_version}"
  subnets                                      = "${var.private_subnets}"
  vpc_id                                       = "${var.vpc_id}"
  kubeconfig_aws_authenticator_additional_args = ["-r", "${var.deploy_assume_role_arn}"]
  worker_additional_security_group_ids         = ["${var.additional_sg}"]
  workers_additional_policies_count            = 4
  workers_additional_policies                  = ["arn:aws:iam::984475896453:policy/ExternalDNS", "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM", "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly", "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy", "arn:aws:iam::984475896453:policy/ClusterAutoscale"]
  cluster_enabled_log_types                    = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  map_roles_count                              = 2
  map_roles = [
    {
      role_arn = "${var.mgmt_node_arn}"
      username = "eks-admin"
      group = "system:masters"
    },
    {
      role_arn = "arn:aws:iam::984475896453:role/forte-dev-eks-mgmt"
      username = "eks-admin"
      group = "system:masters"
    }
  ]

  worker_group_count                           = "4"
  worker_groups = [
    {
      instance_type                 = "${var.node_instance_type}"
      asg_max_size                  = "${var.node_asg_max}"
      asg_min_size                  = "${var.node_asg_min}"
      asg_desired_capacity          = "${var.node_asg_desired}"
      kubelet_extra_args            = "--node-labels=node-affinity=main,nodegroup=main"
      key_name                      = "${var.key_name}"
      additional_security_group_ids = "${var.worker_additional_sg}"
      autoscaling_enabled           = "true"
      additional_userdata           = <<EOF
      echo 'LimitMEMLOCK=infinity' >> /usr/lib/systemd/system/docker.service
      sudo systemctl daemon-reload
      sudo systemctl restart docker.service
EOF
    },
    {
      instance_type                 = "${var.networking_node_instance_type}"
      asg_max_size                  = "${var.networking_node_asg_max}"
      asg_min_size                  = "${var.networking_node_asg_min}"
      asg_desired_capacity          = "${var.networking_node_asg_desired}"
      kubelet_extra_args            = "--node-labels=node-affinity=networking,nodegroup=networking"
      key_name                      = "${var.key_name}"
      additional_security_group_ids = "${var.networking_worker_additional_sg}"
      autoscaling_enabled           = "true"
      additional_userdata           = <<EOF
      echo 'LimitMEMLOCK=infinity' >> /usr/lib/systemd/system/docker.service
      sudo systemctl daemon-reload
      sudo systemctl restart docker.service
EOF
    },
    {
      instance_type                 = "${var.nway_node_instance_type}"
      asg_max_size                  = "${var.nway_node_asg_max}"
      asg_min_size                  = "${var.nway_node_asg_min}"
      asg_desired_capacity          = "${var.nway_node_asg_desired}"
      kubelet_extra_args            = "--node-labels=node-affinity=nway,nodegroup=nway"
      key_name                      = "${var.key_name}"
      additional_security_group_ids = "${var.nway_worker_additional_sg}"
      autoscaling_enabled           = "true"
      additional_userdata           = <<EOF
      echo 'LimitMEMLOCK=infinity' >> /usr/lib/systemd/system/docker.service
      sudo systemctl daemon-reload
      sudo systemctl restart docker.service
EOF
    },
    {
      instance_type                 = "${var.nway_node_instance_type}"
      asg_max_size                  = "${var.nway_node_asg_max}"
      asg_min_size                  = "${var.nway_node_asg_min}"
      asg_desired_capacity          = "${var.nway_node_asg_desired}"
      kubelet_extra_args            = "--node-labels=node-affinity=rally,nodegroup=rally"
      key_name                      = "${var.key_name}"
      additional_security_group_ids = "${var.nway_worker_additional_sg}"
      autoscaling_enabled           = "true"
      additional_userdata           = <<EOF
      echo 'LimitMEMLOCK=infinity' >> /usr/lib/systemd/system/docker.service
      sudo systemctl daemon-reload
      sudo systemctl restart docker.service
EOF
    }
  ]

  tags = {
    environment = "${var.env}"
    "kubernetes.io/cluster/${var.cluster_name}"  = "owned"
    "k8s.io/cluster-autoscaler/enabled" = "true"
  }
}
