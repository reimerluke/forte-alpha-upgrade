data "aws_region" "current" {}

data "template_file" "mgmt" {
  count = "${var.create_management_instance == "true" ? 1 : 0}"
  
  template = "${file("${path.module}/mgmt.sh")}"

  vars {
    cluster_name = "${var.cluster_name}"
    vpc_id = "${var.vpc_id}"
    env = "${var.env}"
    region = "${data.aws_region.current.name}"
    mgmt_bucket = "${aws_s3_bucket.mgmt.id}"
    mgmt_role = "${aws_iam_role.mgmt.arn}"
  }
}

resource "aws_instance" "mgmt" {
  count = "${var.create_management_instance == "true" ? 1 : 0}"
  depends_on = ["module.eks", "aws_s3_bucket_object.kubeconfig"]
  ami = "${var.mgmt_ami}"
  instance_type = "${var.mgmt_instance_type}"
  key_name = "${var.key_name}"
  vpc_security_group_ids = ["${var.mgmt_security_groups}"]
  subnet_id = "${var.mgmt_subnet}"
  associate_public_ip_address = "${var.mgmt_public_ip}"
  iam_instance_profile = "${aws_iam_instance_profile.mgmt.id}"
  user_data = "${data.template_file.mgmt.rendered}"

  tags = {
    Name = "${var.cluster_name}-mgmt"
  }
}
