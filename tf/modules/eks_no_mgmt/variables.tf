variable "vpc_id" {}
variable "private_subnets" {
  type = "list"
}
variable "env" {}
variable "key_name" {}
variable "node_instance_type" {
  default = "t3.medium"
}
variable "networking_node_instance_type" {
  default = "t3.medium"
}
variable "node_asg_max" {
  default = 2
}
variable "node_asg_min" {
  default = 2
}
variable "node_asg_desired" {
  default = 2
}
variable "networking_node_asg_max" {
  default = 2
}
variable "networking_node_asg_min" {
  default = 2
}
variable "networking_node_asg_desired" {
  default = 2
}
variable "nway_node_asg_max" {
  default = 2
}
variable "nway_node_asg_min" {
  default = 2
}
variable "nway_node_asg_desired" {
  default = 2
}
variable "nway_worker_additional_sg" {
  description = "Additional worker node SG"
  #  type = "list"
}
variable "nway_node_instance_type" {
  default = "t3.medium"
}
variable "rally_node_asg_max" {
  default = 2
}
variable "rally_node_asg_min" {
  default = 2
}
variable "rally_node_asg_desired" {
  default = 2
}
variable "rally_worker_additional_sg" {
  description = "Additional worker node SG"
  #  type = "list"
}
variable "rally_node_instance_type" {
  default = "t3.medium"
}
variable "cluster_name" {}
variable "deploy_assume_role_arn" {}
variable "cluster_version" {
  default = "1.14"
}
variable "create_management_instance" {
  description = "Whether or not to create management instance"
  default = "true"
}
variable "mgmt_ami" {
  default = ""
}
variable "mgmt_instance_type" {
  default = "t3.micro"
}
variable "mgmt_subnet" {
  default = ""
}
variable "mgmt_security_groups" {
  default = []
}
variable "mgmt_public_ip" {
  default = true
}
variable "mgmt_node_arn" {
  description = "Management Role ARN if using existing management instance"
  default = ""
}
variable "worker_additional_sg" {
  description = "Additional worker node SG"
#  type = "list"
}
variable "networking_worker_additional_sg" {
  description = "Additional worker node SG"
#  type = "list"
}
variable "additional_sg" {
  description = "Additional SG to all nodes"
#  type = "list"
}
