resource "aws_s3_bucket" "mgmt" {
  bucket_prefix = "${var.cluster_name}-mgmt"
  acl = "private"
}

resource "aws_s3_bucket_object" "kubeconfig" {
  bucket  = "${aws_s3_bucket.mgmt.id}"
  key     = "kubeconfig.yml"
  content = "${module.eks.kubeconfig}"
}
