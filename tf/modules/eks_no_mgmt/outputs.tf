output "mgmt_ip" {
  value = "${aws_instance.mgmt.*.public_ip}"
}

output "cluster_id" {
  value = "${module.eks.cluster_id}"
}

output "eks_worker_sg" {
  value = "${module.eks.worker_security_group_id}"
}

output "mgmt_node_arn" {
  value = "${aws_iam_role.mgmt.*.arn}"
}
