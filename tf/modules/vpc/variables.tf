# VPC Meta
variable "name" {
  default = "vpc"
  description = "Name of the VPC"
}
variable "cidr" {
  description = "CIDR block the VPC will use"
}
variable "azs" {
  type = "list"
  description = "List of availability zones to place the subnets in"
}
variable "public_subnet_suffix" {
  default = "public"
  description = "Suffix to append to public subnets name"
}
variable "private_subnet_suffix" {
  default = "private"
  description = "Suffix to append to private subnets name"
}
variable "database_subnet_suffix" {
  default = "db"
  description = "Suffix to append to database subnets name"
}
variable "intra_subnet_suffix" {
  default = "intra"
  description = "Suffix to append to intra subnets name"
}
variable "vpc_tags" {
  description = "Additional tags for the VPC"
  default     = {}
}
variable "public_subnet_tags" {
  description = "Additional tags for the public subnets"
  default     = {}
}
variable "private_subnet_tags" {
  description = "Additional tags for the private subnets"
  default     = {}
}
variable "database_subnet_tags" {
  description = "Additional tags for the database subnets"
  default     = {}
}
variable "intra_subnet_tags" {
  description = "Additional tags for the intra subnets"
  default     = {}
}
variable "tags" {
  description = "A map of tags to add to all resources"
  default     = {}
}

# NAT
variable "single_nat_gateway" {
  description = "Should be true if you want to provision a single shared NAT Gateway across all of your private networks"
  default = false
}
variable "one_nat_gateway_per_az" {
  description = "Should be true if you want only one NAT Gateway per availability zone. Requires the vars `azs` to be set, and the number of `public_subnets` created to be greater than or equal to the number of availability zones specified in `azs`"
  default = false
}
variable "enable_nat_gateway" {
  description = "Should be true if you want to enable NAT Gateway."
  default = true
}

# Public  Subnets
variable "create_public_subnets" {
  default = true
}
variable "map_public_ip_on_launch" {
  default = true
}
variable "public_subnets" {
  type = "list"
  default = []
  description = "CIDRs of the public subnets"
}

# Private Subnets
variable "create_private_subnets" {
  default = true
}
variable "private_subnets" {
  type = "list"
  default = []
  description = "CIDRs of the private subnets"
}

# Database Subnets
variable "create_database_subnets" {
  default = true
}
variable "database_subnets" {
  type = "list"
  default = []
  description = "CIDRs of the database subnets"
}
variable "create_database_subnet_group" {
  default = false
}

# Intra Subnets (no public routes)
variable "create_intra_subnets" {
  default = true
}
variable "intra_subnets" {
  type = "list"
  default = []
  description = "CIDRs of the intra subnets"
}

# VPC Endpoints
variable "vpc_endpoint_sgs" {
  type = "list"
  default = []
  description = "Security group for the Interface type VPC endpoints"
}
variable "vpc_endpoint_subnet_ids" {
  type = "list"
  default = []
  description = "Subnets to place the Interface type VPC Endpoints"
}
variable "enable_s3_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_dynamodb_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_sqs_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_sns_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_ec2_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_ec2messages_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_ssm_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_ssmmessages_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_kms_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_ecs_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_elasticloadbalancing_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_cloudwatchlogs_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_cloudwatchmonitoring_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_cloudwatchevents_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_cloudtrail_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_ecr_dkr_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_ecr_api_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}
variable "enable_api_gateway_endpoint" {
  default = false
  description = "Should be set to `true` if you want to enable the this endpoint"
}

# Flow Logs
variable "enable_vpc_flow_logs" {
  default = true
  description = "Should be set to `true` if you want to enable the VPC Flow Logs"
}

# DHCP Options Set
variable "enable_dhcp_options" {
  description = "Should be true if you want to specify a DHCP options set with a custom domain name, DNS servers, NTP servers, netbios servers, and/or netbios server type"
  default     = false
}
variable "dhcp_options_domain_name" {
  description = "Specifies DNS name for DHCP options set (requires enable_dhcp_options set to true)"
  default     = ""
}
variable "dhcp_options_domain_name_servers" {
  description = "Specify a list of DNS server addresses for DHCP options set, default to AWS provided (requires enable_dhcp_options set to true)"
  default = ["AmazonProvidedDNS"]
}
variable "dhcp_options_ntp_servers" {
  description = "Specify a list of NTP servers for DHCP options set (requires enable_dhcp_options set to true)"
  default = []
}
variable "dhcp_options_netbios_name_servers" {
  description = "Specify a list of netbios servers for DHCP options set (requires enable_dhcp_options set to true)"
  default = []
}
variable "dhcp_options_netbios_node_type" {
  description = "Specify netbios node_type for DHCP options set (requires enable_dhcp_options set to true)"
  default     = ""
}
variable "dhcp_options_tags" {
  description = "Additional tags for the DHCP option set (requires enable_dhcp_options set to true)"
  default     = {}
}
