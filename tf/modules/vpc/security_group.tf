# Have terraform take over default SG
resource "aws_default_security_group" "default" {
  vpc_id = "${aws_vpc.vpc.id}"

  ingress {
    protocol  = "tcp"
    self      = true
    from_port = 22
    to_port   = 22
  }
  ingress {
    protocol  = "tcp"
    self      = true
    from_port = 3389
    to_port   = 3389
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(map("Name", "default"), var.tags)}"
}
