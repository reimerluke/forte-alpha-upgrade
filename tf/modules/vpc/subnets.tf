# Public Subnets
resource "aws_subnet" "public" {
  count = "${var.create_public_subnets && length(var.public_subnets) > 0 ? length(var.public_subnets) : 0}"

  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${element(concat(var.public_subnets, list("")), count.index)}"
  availability_zone       = "${element(var.azs, count.index)}"
  map_public_ip_on_launch = "${var.map_public_ip_on_launch}"

  tags = "${merge(map("Name", format("%s-${var.public_subnet_suffix}-%s", var.name, element(var.azs, count.index))), var.tags, var.public_subnet_tags)}"
}

# Private Subnets
resource "aws_subnet" "private" {
  count = "${var.create_private_subnets && length(var.private_subnets) > 0 ? length(var.private_subnets) : 0}"

  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${element(concat(var.private_subnets, list("")), count.index)}"
  availability_zone       = "${element(var.azs, count.index)}"

  tags = "${merge(map("Name", format("%s-${var.private_subnet_suffix}-%s", var.name, element(var.azs, count.index))), var.tags, var.private_subnet_tags)}"
}

# Database Subnets
resource "aws_subnet" "database" {
  count = "${var.create_database_subnets && length(var.database_subnets) > 0 ? length(var.database_subnets) : 0}"

  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${element(concat(var.database_subnets, list("")), count.index)}"
  availability_zone       = "${element(var.azs, count.index)}"

  tags = "${merge(map("Name", format("%s-${var.database_subnet_suffix}-%s", var.name, element(var.azs, count.index))), var.tags, var.database_subnet_tags)}"
}

# Database subnet group (optional)
resource "aws_db_subnet_group" "database" {
  count = "${var.create_database_subnets && length(var.database_subnets) > 0 && var.create_database_subnet_group ? 1 : 0}"

  name        = "${lower(var.name)}"
  description = "Database subnet group for ${var.name}"
  subnet_ids  = ["${aws_subnet.database.*.id}"]

  tags = "${merge(map("Name", format("%s", var.name)), var.tags)}"
}

# Intra Subnets
resource "aws_subnet" "intra" {
  count = "${var.create_intra_subnets && length(var.intra_subnets) > 0 ? length(var.intra_subnets) : 0}"

  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${element(concat(var.intra_subnets, list("")), count.index)}"
  availability_zone       = "${element(var.azs, count.index)}"

  tags = "${merge(map("Name", format("%s-${var.intra_subnet_suffix}-%s", var.name, element(var.azs, count.index))), var.tags, var.intra_subnet_tags)}"
}
