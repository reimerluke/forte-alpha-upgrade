# IAM Role, Policy and Instance Profile for CloudWatch logs
resource "aws_iam_role" "flow_logs" {
  count = "${var.enable_vpc_flow_logs ? 1 : 0}"

  name = "${var.name}-flow-logs"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "ec2.amazonaws.com",
          "vpc-flow-logs.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

# IAM Role Policy
resource "aws_iam_role_policy" "flow_logs" {
  count = "${var.enable_vpc_flow_logs ? 1 : 0}"

  name = "${var.name}-flow-logs"
  role = "${aws_iam_role.flow_logs.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams",
        "cloudwatch:PutMetricData",
        "ec2:DescribeTags",
        "ec2messages:*"

      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

# Instance Profile
resource "aws_iam_instance_profile" "flow_logs" {
  count = "${var.enable_vpc_flow_logs ? 1 : 0}"

  name = "${var.name}-flow-logs"
  role = "${aws_iam_role.flow_logs.name}"
}

# CloudWatch Log Group
resource "aws_cloudwatch_log_group" "flow_logs" {
  count = "${var.enable_vpc_flow_logs ? 1 : 0}"

  name = "${var.name}-flow-logs"
}

# Flow Logs
resource "aws_flow_log" "flow_logs" {
  count = "${var.enable_vpc_flow_logs ? 1 : 0}"

  iam_role_arn    = "${aws_iam_role.flow_logs.arn}"
  log_destination = "${aws_cloudwatch_log_group.flow_logs.arn}"
  traffic_type    = "ALL"
  vpc_id          = "${aws_vpc.vpc.id}"
}
