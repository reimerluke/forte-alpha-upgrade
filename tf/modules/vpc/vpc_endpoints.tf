# S3 Endpoint
resource "aws_vpc_endpoint" "s3" {
  count = "${var.enable_s3_endpoint ? 1 : 0}"

  vpc_id       = "${aws_vpc.vpc.id}"
  service_name = "com.amazonaws.${data.aws_region.current.name}.s3"
}

# S3 Endpoint Route Table Associations
resource "aws_vpc_endpoint_route_table_association" "public_s3" {
  count = "${var.enable_s3_endpoint && length(var.public_subnets) > 0 ? 1 : 0}"

  vpc_endpoint_id = "${aws_vpc_endpoint.s3.id}"
  route_table_id  = "${aws_route_table.public.id}"
}
resource "aws_vpc_endpoint_route_table_association" "private_s3" {
  count = "${var.enable_s3_endpoint && length(var.private_subnets) > 0 ? local.nat_gateway_count : 0}"

  vpc_endpoint_id = "${aws_vpc_endpoint.s3.id}"
  route_table_id  = "${element(aws_route_table.private.*.id, count.index)}"
}
resource "aws_vpc_endpoint_route_table_association" "database_s3" {
  count = "${var.enable_s3_endpoint && length(var.database_subnets) > 0 ? local.nat_gateway_count : 0}"

  vpc_endpoint_id = "${aws_vpc_endpoint.s3.id}"
  route_table_id  = "${element(aws_route_table.database.*.id, count.index)}"
}
resource "aws_vpc_endpoint_route_table_association" "intra_s3" {
  count = "${var.enable_s3_endpoint && length(var.intra_subnets) > 0 ? 1 : 0}"

  vpc_endpoint_id = "${aws_vpc_endpoint.s3.id}"
  route_table_id  = "${aws_route_table.intra.id}"
}

# DynamoDB Endpoint
resource "aws_vpc_endpoint" "dynamodb" {
  count = "${var.enable_dynamodb_endpoint ? 1 : 0}"

  vpc_id       = "${aws_vpc.vpc.id}"
  service_name = "com.amazonaws.${data.aws_region.current.name}.dynamodb"
}

# DynamoDB Endpoint Route Table Associations
resource "aws_vpc_endpoint_route_table_association" "public_dynamodb" {
  count = "${var.enable_dynamodb_endpoint && length(var.public_subnets) > 0 ? 1 : 0}"

  vpc_endpoint_id = "${aws_vpc_endpoint.dynamodb.id}"
  route_table_id  = "${aws_route_table.public.id}"
}
resource "aws_vpc_endpoint_route_table_association" "private_dynamodb" {
  count = "${var.enable_dynamodb_endpoint && length(var.private_subnets) > 0 ? local.nat_gateway_count : 0}"

  vpc_endpoint_id = "${aws_vpc_endpoint.dynamodb.id}"
  route_table_id  = "${element(aws_route_table.private.*.id, count.index)}"
}
resource "aws_vpc_endpoint_route_table_association" "database_dynamodb" {
  count = "${var.enable_dynamodb_endpoint && length(var.database_subnets) > 0 ? local.nat_gateway_count : 0}"

  vpc_endpoint_id = "${aws_vpc_endpoint.dynamodb.id}"
  route_table_id  = "${element(aws_route_table.database.*.id, count.index)}"
}
resource "aws_vpc_endpoint_route_table_association" "intra_dynamodb" {
  count = "${var.enable_dynamodb_endpoint && length(var.intra_subnets) > 0 ? 1 : 0}"

  vpc_endpoint_id = "${aws_vpc_endpoint.dynamodb.id}"
  route_table_id  = "${aws_route_table.intra.id}"
}

# SNS Endpoint
resource "aws_vpc_endpoint" "sns" {
  count = "${var.enable_sns_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.sns"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# SQS Endpoint
resource "aws_vpc_endpoint" "sqs" {
  count = "${var.enable_sqs_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.sqs"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# EC2 Endpoint
resource "aws_vpc_endpoint" "ec2" {
  count = "${var.enable_ec2_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.ec2"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# EC2 Messages Endpoint
resource "aws_vpc_endpoint" "ec2messages" {
  count = "${var.enable_ec2messages_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.ec2messages"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# SSM Endpoint
resource "aws_vpc_endpoint" "ssm" {
  count = "${var.enable_ssm_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.ssm"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# SSM Messages Endpoint
resource "aws_vpc_endpoint" "ssmmessages" {
  count = "${var.enable_ssmmessages_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.ssmmessages"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# KMS Endpoint
resource "aws_vpc_endpoint" "kms" {
  count = "${var.enable_kms_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.kms"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# ECS Endpoint
resource "aws_vpc_endpoint" "ecs" {
  count = "${var.enable_ecs_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.ecs"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# Elastic Load Balancing Endpoint
resource "aws_vpc_endpoint" "elasticloadbalancing" {
  count = "${var.enable_elasticloadbalancing_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.elasticloadbalancing"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# CloudWatch Logs Endpoint
resource "aws_vpc_endpoint" "logs" {
  count = "${var.enable_cloudwatchlogs_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.logs"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# CloudWatch Monitoring Endpoint
resource "aws_vpc_endpoint" "monitoring" {
  count = "${var.enable_cloudwatchmonitoring_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.monitoring"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# CloudWatch Events Endpoint
resource "aws_vpc_endpoint" "events" {
  count = "${var.enable_cloudwatchevents_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.events"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# CloudTrail Endpoint
resource "aws_vpc_endpoint" "cloudtrail" {
  count = "${var.enable_cloudtrail_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.cloudtrail"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# ECR DKR Endpoint
resource "aws_vpc_endpoint" "ecr_dkr" {
  count = "${var.enable_ecr_dkr_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.ecr.dkr"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# ECR API Endpoint
resource "aws_vpc_endpoint" "ecr_api" {
  count = "${var.enable_ecr_api_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.ecr.api"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}

# API Gateway Endpoint
resource "aws_vpc_endpoint" "api_gateway" {
  count = "${var.enable_api_gateway_endpoint ? 1 : 0}"

  vpc_id             =  "${aws_vpc.vpc.id}"
  service_name       =  "com.amazonaws.${data.aws_region.current.name}.execute-api"
  vpc_endpoint_type  =  "Interface"
  auto_accept        =  true
  subnet_ids         = ["${coalescelist(var.vpc_endpoint_subnet_ids, aws_subnet.public.*.id)}"]
  security_group_ids = ["${aws_default_security_group.default.id}", "${var.vpc_endpoint_sgs}"]
}
