# Public Route Table
resource "aws_route_table" "public" {
  count = "${var.create_public_subnets && length(var.public_subnets) > 0 ? 1 : 0}"

  vpc_id = "${aws_vpc.vpc.id}"

  tags = "${merge(map("Name", format("%s-${var.public_subnet_suffix}", var.name)), var.tags)}"
}
# Route for Public Subnets and Internet Gateway
resource "aws_route" "public_internet_gateway" {
  count = "${var.create_public_subnets && length(var.public_subnets) > 0 ? 1 : 0}"

  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.igw.id}"

  timeouts {
    create = "5m"
  }
}

# Private Route Table
resource "aws_route_table" "private" {
  count = "${var.create_private_subnets && length(var.private_subnets) > 0 ? local.nat_gateway_count : 0}"

  vpc_id = "${aws_vpc.vpc.id}"

  tags = "${merge(map("Name", format("%s-${var.private_subnet_suffix}-%s", var.name, element(var.azs, count.index))), var.tags)}"
}
# Route for Private Subnets and NAT Gateway
resource "aws_route" "private_nat_gateway" {
  count                  = "${var.create_private_subnets && length(var.private_subnets) > 0 && var.enable_nat_gateway ? local.nat_gateway_count : 0}"

  route_table_id         = "${element(aws_route_table.private.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${element(aws_nat_gateway.nat.*.id, count.index)}"

  timeouts {
    create = "5m"
  }
}

# Database Route Table
resource "aws_route_table" "database" {
  count = "${var.create_database_subnets && length(var.database_subnets) > 0 ? local.nat_gateway_count : 0}"

  vpc_id = "${aws_vpc.vpc.id}"

  tags = "${merge(map("Name", format("%s-${var.database_subnet_suffix}-%s", var.name, element(var.azs, count.index))), var.tags)}"
}
# Route for Database Subnets and NAT Gateway (Optional)
resource "aws_route" "database_nat_gateway" {
  count                  = "${var.create_database_subnets && length(var.database_subnets) > 0 && var.enable_nat_gateway ? local.nat_gateway_count : 0}"

  route_table_id         = "${element(aws_route_table.database.*.id, count.index)}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${element(aws_nat_gateway.nat.*.id, count.index)}"

  timeouts {
    create = "5m"
  }
}

# Intra Route Table
resource "aws_route_table" "intra" {
  count = "${var.create_intra_subnets && length(var.intra_subnets) > 0 ? 1 : 0}"

  vpc_id = "${aws_vpc.vpc.id}"

  tags = "${merge(map("Name", format("%s-${var.intra_subnet_suffix}", var.name)), var.tags)}"
}

# Route Table Associations
resource "aws_route_table_association" "public" {
  count = "${var.create_public_subnets && length(var.public_subnets) > 0 ? length(var.public_subnets) : 0}"

  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}
resource "aws_route_table_association" "private" {
  count = "${var.create_private_subnets && length(var.private_subnets) > 0 ? length(var.private_subnets) : 0}"

  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, (var.single_nat_gateway ? 0 : count.index))}"
}
resource "aws_route_table_association" "database" {
  count = "${var.create_database_subnets && length(var.database_subnets) > 0 ? length(var.database_subnets) : 0}"

  subnet_id      = "${element(aws_subnet.database.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.database.*.id, (var.single_nat_gateway ? 0 : count.index))}"
}

resource "aws_route_table_association" "intra" {
  count = "${var.create_intra_subnets && length(var.intra_subnets) > 0 ? length(var.intra_subnets) : 0}"

  subnet_id      = "${element(aws_subnet.intra.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.intra.*.id, count.index)}"
}
