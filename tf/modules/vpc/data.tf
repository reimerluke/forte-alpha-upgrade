# Setting Terraform required version to deploy this module
terraform {
  required_version = ">= 0.10.3"
}

# Handling the NAT setup
locals {
  max_subnet_length = "${max(length(var.private_subnets), length(var.database_subnets))}"
  nat_gateway_count = "${var.single_nat_gateway ? 1 : (var.one_nat_gateway_per_az ? length(var.azs) : local.max_subnet_length)}"
}

# Getting the current region
data "aws_region" "current" {}
