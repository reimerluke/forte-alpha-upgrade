# Build the base VPC
resource "aws_vpc" "vpc" {
  cidr_block = "${var.cidr}"
  enable_dns_hostnames = true

  tags = "${merge(map("Name", format("%s", var.name)), var.tags, var.vpc_tags)}"
}

# Build an internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = "${merge(map("Name", format("%s-igw", var.name)), var.tags)}"
}

# Build NAT gateway
resource "aws_nat_gateway" "nat" {
  count = "${var.enable_nat_gateway ? local.nat_gateway_count : 0}"

  allocation_id = "${element(aws_eip.nat.*.id, (var.single_nat_gateway ? 0 : count.index))}"
  subnet_id     = "${element(aws_subnet.public.*.id, (var.single_nat_gateway ? 0 : count.index))}"

  tags = "${merge(map("Name", format("%s-nat-%s", var.name, element(var.azs, (var.single_nat_gateway ? 0 : count.index)))), var.tags)}"

  depends_on = ["aws_internet_gateway.igw"]
}

# Build EIP for NAT gateway
resource "aws_eip" "nat" {
  count = "${var.enable_nat_gateway ? local.nat_gateway_count : 0}"

  vpc = true

  tags = "${merge(map("Name", format("%s-nat-%s", var.name, element(var.azs, (var.single_nat_gateway ? 0 : count.index)))), var.tags)}"
}

# DHCP Options Set
resource "aws_vpc_dhcp_options" "dhcp" {
  count = "${var.enable_dhcp_options ? 1 : 0}"

  domain_name          = "${var.dhcp_options_domain_name}"
  domain_name_servers  = ["${var.dhcp_options_domain_name_servers}"]
  ntp_servers          = ["${var.dhcp_options_ntp_servers}"]
  netbios_name_servers = ["${var.dhcp_options_netbios_name_servers}"]
  netbios_node_type    = "${var.dhcp_options_netbios_node_type}"

  tags = "${merge(map("Name", format("%s", var.name)), var.tags, var.dhcp_options_tags)}"
}

# DHCP Options Set Association
resource "aws_vpc_dhcp_options_association" "dhcp" {
  count = "${var.enable_dhcp_options ? 1 : 0}"

  vpc_id          = "${aws_vpc.vpc.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.dhcp.id}"
}
