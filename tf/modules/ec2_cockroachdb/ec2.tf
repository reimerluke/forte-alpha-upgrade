# EC2 Instance
resource "aws_instance" "cockroachdb" {
  ami                     =  "${var.ec2_ami}"
  instance_type           =  "${var.ec2_instance_type}"
  key_name                =  "${var.ec2_key_name}"
  iam_instance_profile    =  "${aws_iam_instance_profile.cockroachdb.name}"

  subnet_id               =  "${var.ec2_subnet_id}"
  vpc_security_group_ids  = ["${var.ec2_security_groups}"]

  ebs_optimized           = "${var.ebs_optimized}"

  root_block_device {
    volume_type           = "${var.ec2_volume_type}"
    volume_size           = "${var.ec2_volume_size}"
  }

  tags {
    Name                  = "${var.ec2_name}"
    Environment           = "${terraform.workspace}"
    Terraform             = "yes"
  }
}

resource "aws_route53_record" "this" {
  zone_id = "${var.hosted_zone_id}"
  name    = "${var.dns_name}"
  type    = "A"
  ttl     = "120"

  records = ["${aws_instance.cockroachdb.private_ip}"]
}
