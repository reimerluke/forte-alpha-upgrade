# IAM Role
resource "aws_iam_role" "cockroachdb" {
  name_prefix = "${var.ec2_name}"
  path = "/"
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": [
                 "ec2.amazonaws.com",
                 "ssm.amazonaws.com"
               ]
            },
            "Effect": "Allow"
        }
    ]
}
EOF
}

# IAM Role Policy
resource "aws_iam_role_policy" "cockroachdb" {
  name = "${var.ec2_name}"
  role = "${aws_iam_role.cockroachdb.name}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowCloudwatchLogStreams",
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams",
        "cloudwatch:PutMetricData",
        "ec2:Describe*",
        "ec2messages:*"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

# Including SSM policy to enable the automaintenance module
resource "aws_iam_role_policy_attachment" "ssm-cockroachdb" {
  role       = "${aws_iam_role.cockroachdb.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

# IAM Instance Profile
resource "aws_iam_instance_profile" "cockroachdb" {
    name = "${var.ec2_name}"
    role = "${aws_iam_role.cockroachdb.name}"
}
