variable "ec2_name" {}
variable "ec2_ami" {}
variable "ec2_key_name" {}
variable "ec2_instance_type" {}
variable "ec2_security_groups" {
  type = "list"
}
variable "ec2_subnet_id" {}
variable "ec2_vpc_id" {}
variable "ebs_optimized"{
  default = false
}
variable "ec2_volume_type" {
  default = "gp2"
}
variable "ec2_volume_size" {
  default = 100
}
variable "hosted_zone_id" {
  default = "Z1YIN7ED8XCO7V"
  description = "Route53 hosted zone"
}

variable "dns_name" {
  description = "DNS Entry"
}
