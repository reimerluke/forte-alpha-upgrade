output "role_arn" {
  value = "${aws_iam_role.cockroachdb.arn}"
}
output "ec2_instance_id" {
  value = "${aws_instance.cockroachdb.id}"
}
