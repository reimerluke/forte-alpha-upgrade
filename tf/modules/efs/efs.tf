data "aws_availability_zones" "available" {}

resource "aws_efs_file_system" "this" {
  count                           = "${var.enabled ? 1 : 0}"
  creation_token                  = "${var.efs_name}"
  tags                            = "${var.tags}"
  encrypted                       = "${var.encrypted}"
  performance_mode                = "${var.performance_mode}"
  provisioned_throughput_in_mibps = "${var.provisioned_throughput_in_mibps}"
  throughput_mode                 = "${var.throughput_mode}"
}

resource "aws_efs_mount_target" "this" {
  count           = "${var.mount_target_enabled && length(var.azs) > 0 ? length(var.azs) : 0}"
  file_system_id  = "${join("", aws_efs_file_system.this.*.id)}"
  ip_address      = "${var.mount_target_ip_address}"
  subnet_id       = "${element(var.subnets, count.index)}"
  security_groups = ["${join("", aws_security_group.this.*.id)}", "${var.additional_security_group_ids}"]
}

resource "aws_security_group" "this" {
  count       = "${var.enabled ? 1 : 0}"
  name        = "${var.efs_name}-sg"
  description = "EFS"
  vpc_id      = "${var.vpc_id}"

  lifecycle {
    create_before_destroy = true
  }

  ingress {
    from_port       = "2049"                     # NFS
    to_port         = "2049"
    protocol        = "tcp"
    security_groups = ["${var.security_groups}", "${var.additional_security_group_ids}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${var.tags}"
}
