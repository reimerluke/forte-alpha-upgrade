variable "efs_name" {
  description = "The name of the Elastic File System"
  type        = "string"
}

variable "enabled" {
  description = "Set to false to prevent the module from creating anything"
  default     = true
}

variable "mount_target_enabled" {
  description = "Set to false to prevent the module from creating anything"
  default     = true
}

variable "encrypted" {
  description = "If true, the disk will be encrypted"
  type        = "string"
  default     = "false"
}

variable "ingress_cidr" {
  description = "List of CIDR to allow access to EFS"
  type        = "list"
  default     = []
}

variable "vpc_id" {
  default = ""
  description = "VPC ID"
}

variable "azs" {
  type = "list"
  description = "List of availability zones to place the subnets in"
}

variable "additional_security_group_ids" {
  description = "Additional security groups to allow EFS"
}

variable "kms_key_id" {
  description = "ARN for the KMS encryption key. When specifying kms_key_id, encrypted needs to be set to true"
  type        = "string"
  default     = ""
}

variable "performance_mode" {
  type        = "string"
  description = "The file system performance mode. Can be either `generalPurpose` or `maxIO`"
  default     = "generalPurpose"
}

variable "provisioned_throughput_in_mibps" {
  default     = 0
  description = "The throughput, measured in MiB/s, that you want to provision for the file system. Only applicable with throughput_mode set to provisioned"
}

variable "throughput_mode" {
  type        = "string"
  description = "Throughput mode for the file system. Defaults to bursting. Valid values: bursting, provisioned. When using provisioned, also set provisioned_throughput_in_mibps"
  default     = "bursting"
}

variable "mount_target_ip_address" {
  type        = "string"
  description = "The address (within the address range of the specified subnet) at which the file system may be mounted via the mount target"
  default     = ""
}

variable "region" {
  description = "AWS region"
  type        = "string"
  default     = ""
}

variable "security_groups" {
  description = "AWS security group IDs to allow to connect to the EFS"
  type        = "list"
  default     = []
}

variable "subnets" {
  description = "AWS subnet IDs"
  type        = "list"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = "map"
  default     = {}
}
