output "peering_connection_id" {
  value = "${ join("", aws_vpc_peering_connection.peering.*.id) }"
  description = "The ID of the peering connection"
}
