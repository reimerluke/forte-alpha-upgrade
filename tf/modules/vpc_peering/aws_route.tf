# Destination VPC Routes
resource "aws_route" "peer_src_to_peer_dst" {
  count = "${length(var.peer_src_route_tables)}"

  route_table_id            = "${element(var.peer_src_route_tables, count.index)}"
  destination_cidr_block    = "${var.peer_dst_vpc_cidr}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.peering.id}"
}
# Source VPC Routes
resource "aws_route" "peer_dst_to_peer_src" {
  count = "${length(var.peer_dst_route_tables)}"

  route_table_id            = "${element(var.peer_dst_route_tables, count.index)}"
  destination_cidr_block    = "${var.peer_src_vpc_cidr}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.peering.id}"
}
