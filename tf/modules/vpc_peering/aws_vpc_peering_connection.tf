# Requester's side of the connection.
resource "aws_vpc_peering_connection" "peering" {
  count = "${var.is_requester ? 1 : 0}"

  peer_vpc_id   = "${var.peer_dst_vpc_id}"
  vpc_id        = "${var.peer_src_vpc_id}"

  auto_accept = "${var.auto_accept}"

  tags = "${merge(map("Name", "${var.peering_name}"), var.tags)}"
}
