variable "peering_name" {
  default = ""
  description = "The name of the VPC peering connection"
}

variable "peer_src_vpc_id" {
  type = "string"
  default = ""
  description = "The VPC to peer from"
}

variable "peer_src_route_tables" {
  type = "list"
  default = []
  description = "List of route tables from the src VPC"
}

variable "peer_src_vpc_cidr" {
  type = "string"
  default = ""
  description = "CIDR from the src to VPC to include in the routes"
}

variable "peer_dst_vpc_id" {
  type = "string"
  default = ""
  description = "The VPC ID to peer to"
}

variable "peer_dst_route_tables" {
  type = "list"
  default = []
  description = "List of route tables from the peer VPC"
}

variable "peer_dst_vpc_cidr" {
  type = "string"
  default = ""
  description = "CIDR from the peer to VPC to include in the routes"
}

variable "auto_accept" {
  type = "string"
  description = "Specify whether or not connections should be automatically accepted"
  default = true
}

variable "is_requester" {
  default = true
  description = "Should be set to `false` if you don't need to create the peering, only update the routes"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  default = {}
}
