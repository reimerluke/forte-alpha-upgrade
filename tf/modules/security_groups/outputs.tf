output "sg_internal" {
  value = "${aws_security_group.internal.id}"
}
output "sg_app" {
  value = "${aws_security_group.app.id}"
}
output "sg_web" {
  value = "${aws_security_group.web.id}"
}
output "sg_postgres" {
  value = "${aws_security_group.postgres.id}"
}
output "sg_cockroachdb" {
  value = "${aws_security_group.cockroachdb.id}"
}
output "sg_redis" {
  value = "${aws_security_group.redis.id}"
}

output "sg_mainnet" {
  value = "${aws_security_group.mainnet.id}"
}

output "sg_eks_lb" {
  value = "${aws_security_group.eks_lb.id}"
}
