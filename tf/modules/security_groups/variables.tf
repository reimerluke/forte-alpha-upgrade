variable "vpc_id" {}
variable "vpc_cidr" {
  default = ""
}
variable "peer_vpc_cidr" {
  default = ""
}
variable "tags" {
  description = "A map of tags to add to all resources"
  default     = {}
}
variable "openvpn_address" {
  default = ""
}

variable "mgmt_vpc_cidr" {
  description = "Mgmt VPC cidr"
  default     = ""
}

variable "eks_mgmt_address" {
  description = "address for the eks mgmt instance"
}

variable "eks_worker_sg" {
  description = "EKS worker node security group"
}

variable "mainnet_ingress_cidr" {
  default     = ""
  description = "Cidr block allowing full ingress to mainnet instance"
}
