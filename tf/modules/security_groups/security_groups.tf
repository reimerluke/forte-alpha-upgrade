# Internal
resource "aws_security_group" "internal" {
  name        = "${terraform.workspace}-internal"
  description = "Allow traffic from peer VPC"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${var.peer_vpc_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(map("Name", "${terraform.workspace}-internal"), var.tags)}"
}

# Web Public
resource "aws_security_group" "web" {
  name        = "${terraform.workspace}-web"
  description = "Allow inbound public web traffic"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(map("Name", "${terraform.workspace}-web-public"), var.tags)}"
}

# App
resource "aws_security_group" "app" {
  name        = "${terraform.workspace}-app"
  description = "Allow inbound app traffic"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    self      = true
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    self      = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(map("Name", "${terraform.workspace}-app"), var.tags)}"
}

# PostgreSQL
resource "aws_security_group" "postgres" {
  name        = "${terraform.workspace}-postgres"
  description = "Allow inbound PostgreSQL traffic"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    self      = true
  }
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["${var.openvpn_address}"]
    description = "OpenVPN Address"
  }

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["${var.eks_mgmt_address}"]
    description = "eks-mgmt instance"
  }

  ingress {
    from_port       = 5432
    to_port         = 5432
    protocol        = "tcp"
    security_groups = ["${var.eks_worker_sg}"]
    description     = "Worker SG"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(map("Name", "${terraform.workspace}-postgres"), var.tags)}"
}

# CockroachDB
resource "aws_security_group" "cockroachdb" {
  name        = "${terraform.workspace}-cockroachdb"
  description = "Allow inbound CockroachDB traffic"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 26257
    to_port     = 26257
    protocol    = "tcp"
    self        = true
    description = "Default port"
  }
  ingress {
    from_port   = 26257
    to_port     = 26257
    protocol    = "tcp"
    cidr_blocks = ["${var.openvpn_address}", "${var.peer_vpc_cidr}", "${var.vpc_cidr}"]
    description = "CockroachDB Port"
  }

  ingress {
    from_port       = 26257
    to_port         = 26257
    protocol        = "tcp"
    security_groups = ["${var.eks_worker_sg}"]
    description     = "Worker SG"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["${var.peer_vpc_cidr}", "${var.vpc_cidr}"]
    description = "Admin UI"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.eks_mgmt_address}"]
    description = "eks-mgmt instance"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(map("Name", "${terraform.workspace}-postgres"), var.tags)}"
}

# Redis
resource "aws_security_group" "redis" {
  name        = "${terraform.workspace}-redis"
  description = "Allow inbound Redis traffic"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port = 6379
    to_port   = 6379
    protocol  = "tcp"
    self      = true
  }
  ingress {
    from_port       = 6379
    to_port         = 6379
    protocol        = "tcp"
    security_groups = ["${aws_security_group.app.id}"]
  }
  ingress {
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    cidr_blocks = ["${var.openvpn_address}"]
    description = "OpenVPN Address"
  }

  ingress {
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    cidr_blocks = ["${var.eks_mgmt_address}"]
    description = "eks-mgmt instance"
  }

  ingress {
    from_port       = 6379
    to_port         = 6379
    protocol        = "tcp"
    security_groups = ["${var.eks_worker_sg}"]
    description     = "Worker SG"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(map("Name", "${terraform.workspace}-redis"), var.tags)}"
}

# Mainnet
resource "aws_security_group" "mainnet" {
  name        = "${terraform.workspace}-mainnet"
  description = "Allow inbound mainnet traffic"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port = 8770
    to_port   = 8771
    protocol  = "tcp"
    self      = true
  }

  ingress {
    from_port   = 8770
    to_port     = 8771
    protocol    = "tcp"
    cidr_blocks = ["${var.mainnet_ingress_cidr}"]
  }

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = ["${var.eks_worker_sg}"]
    description     = "Worker SG"
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${var.openvpn_address}"]
    description = "OpenVPN Address"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(map("Name", "${terraform.workspace}-mainnet"), var.tags)}"
}

# Additional SG for EKS LB
resource "aws_security_group" "eks_lb" {
  name        = "${terraform.workspace}-eks-lb"
  description = "Allow inbound to EKS LBs"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = "${merge(map("Name", "${terraform.workspace}-eks-lb"), var.tags)}"
}
