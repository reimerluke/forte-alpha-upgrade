# This sets up Terraform to use remote state and state file locking
# These are safeguards to ensure we're not overwriting each other's commits
# and trying to push multiple changes at the same time. Read more about it here:
# https://www.terraform.io/docs/state/remote.html

terraform {
  backend "s3" {
    region         = "us-west-2"                                                      # region of the bucket
    bucket         = "terraform-backend-alpha-env-terraformstatebucket-1839zbhcgz8h9" # state file bucket name
    key            = "forte-alpha-04172020/terraform.tfstate"                         # state file key name
    dynamodb_table = "terraform-backend-alpha-env"                                    # name of dynamodb lock table
  }
}
