# Vault Path

locals {
  prefix_env         = "alpha_04172020"
  rds_admin_password = "${data.rds_admin_password}" #github secret / we want it to be eventually
}

# prod-testnet-2 VPC
module "vpc_alpha_04172020" {
  source = "../../modules/vpc"

  name = "vpc-${local.prefix_env}"

  cidr             = "10.210.0.0/16"
  azs              = ["us-west-2a", "us-west-2b", "us-west-2c"]
  public_subnets   = ["10.210.0.0/20", "10.210.16.0/20", "10.210.32.0/20"]
  private_subnets  = ["10.210.48.0/20", "10.210.64.0/20", "10.210.80.0/20"]
  database_subnets = ["10.210.96.0/20", "10.210.112.0/20", "10.210.128.0/20"]
  intra_subnets    = ["10.210.144.0/20", "10.210.160.0/20", "10.210.176.0/20"]

  create_database_subnet_group = true
  single_nat_gateway           = true

  vpc_tags = {
    "kubernetes.io/cluster/${local.prefix_env}-eks" = "shared"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.prefix_env}-eks" = "shared"
    "kubernetes.io/role/elb"                        = "true"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.prefix_env}-eks" = "shared"
    "kubernetes.io/role/internal-elb"               = "true"
  }

  tags = {
    Terraform   = "Yes"
    Environment = "${local.prefix_env}"
  }
}

# Peering Management x prod-testnet-2
module "peering_mgmt_alpha_04172020" {
  source = "../../modules/vpc_peering"

  # Meta
  peering_name = "peering-mgmt-prod-testnet-2"

  # Source
  peer_src_vpc_id   = "vpc-0bce051cb72dd50af"
  peer_src_vpc_cidr = "10.50.0.0/16"
  peer_src_route_tables = ["rtb-0e650a64765b1765b",
  "rtb-0b983e3fdf1d7b8cc"]
  # Destination
  peer_dst_vpc_id   = "${module.vpc_alpha_04172020.vpc_id}"
  peer_dst_vpc_cidr = "${module.vpc_alpha_04172020.vpc_cidr_block}"
  peer_dst_route_tables = ["${module.vpc_alpha_04172020.public_route_table_ids}",
    "${module.vpc_alpha_04172020.private_route_table_ids}",
    "${module.vpc_alpha_04172020.database_route_table_ids}",
  "${module.vpc_alpha_04172020.intra_route_table_ids}"]
  tags = {
    Terraform   = "Yes"
    Environment = "${local.prefix_env}"
  }
}

# Security Groups
module "security_groups" {
  source = "../../modules/security_groups"

  vpc_id               = "${module.vpc_alpha_04172020.vpc_id}"
  vpc_cidr             = "${module.vpc_alpha_04172020.vpc_cidr_block}"
  peer_vpc_cidr        = "10.50.0.0/16"
  openvpn_address      = "10.50.1.172/32"
  eks_mgmt_address     = "10.50.12.144/32"
  eks_worker_sg        = "${module.eks.eks_worker_sg}"
  mainnet_ingress_cidr = "0.0.0.0/0"

  tags = {
    Terraform   = "Yes"
    Environment = "${local.prefix_env}"
  }
}

# Staging RDS PostgreSQL
module "rds_postgres" {
  source = "../../modules/rds_postgres"

  # Meta
  rds_name           = "${local.prefix_env}-postgres"
  rds_instance_type  = "db.r5.4xlarge"
  rds_user           = "postgres"
  rds_password       = "${local.rds_admin_password}"
  rds_bucket         = "${local.prefix_env}-postgres"
  rds_hosted_zone_id = "Z1YIN7ED8XCO7V"
  rds_dns_name       = "${local.prefix_env}-postgres"

  # Networking
  rds_vpc_id             = "${module.vpc_alpha_04172020.vpc_id}"
  rds_subnet_ids         = ["${module.vpc_alpha_04172020.database_subnets}"]
  rds_security_group_ids = ["${module.security_groups.sg_postgres}"]

  # Optional variables
  rds_storage_iops                = 30000
  rds_engine                      = "postgres"
  rds_engine_version              = "11.4"
  rds_engine_version_major        = "11"
  rds_engine_family               = "postgres11"
  rds_publicly_accessible         = "false"
  rds_multi_az                    = "false"
  rds_retention_period            = "14"
  rds_backup_window               = "07:00-07:30"
  rds_maint_window                = "Sat:08:00-Sat:08:30"
  rds_storage_encrypted           = "true"
  rds_storage                     = "600"
  max_allocated_storage           = "800"
  rds_storage_type                = "io1"
  rds_apply_minor_version_upgrade = "false"
  rds_skip_final_snapshot         = "true"
  rds_apply_immediately           = "true"
  rds_deletion_protection         = "true"
  rds_monitoring_interval         = "1"
}

# Staging Elasticache Redis
module "elasticache_redis" {
  source = "../../modules/elasticache_redis"

  ec_name           = "${local.prefix_env}-redis"
  ec_engine         = "redis"
  ec_engine_version = "5.0.4"
  ec_instance_type  = "cache.r4.xlarge"
  ec_replicas       = "1"
  ec_hosted_zone_id = "Z1YIN7ED8XCO7V"
  ec_dns_name       = "${local.prefix_env}-redis"

  vpc_id          = "${module.vpc_alpha_04172020.vpc_id}"
  subnet_ids      = ["${module.vpc_alpha_04172020.private_subnets}"]
  security_groups = ["${module.security_groups.sg_redis}"]
}

# EC2 CockroachDB
module "ec2_cockroachdb" {
  source = "../../modules/ec2_cockroachdb"

  ec2_name          = "${local.prefix_env}-cockroachdb"
  ec2_ami           = "ami-0b2f70d74ac2d9ff7" # Golden image
  ec2_key_name      = "forte-dev-key"
  ec2_instance_type = "c5.4xlarge"
  ec2_security_groups = ["${module.security_groups.sg_internal}",
    "${module.security_groups.sg_web}",
    "${module.security_groups.sg_app}",
  "${module.security_groups.sg_cockroachdb}"]
  ec2_subnet_id   = "${module.vpc_alpha_04172020.database_subnets[1]}"
  ec2_vpc_id      = "${module.vpc_alpha_04172020.vpc_id}"
  ec2_volume_size = "60"
  dns_name        = "${local.prefix_env}-cockroachdb"
}

# EFS
module "efs" {
  source                        = "../../modules/efs"
  enabled                       = true
  mount_target_enabled          = true
  vpc_id                        = "${module.vpc_alpha_04172020.vpc_id}"
  efs_name                      = "${local.prefix_env}-efs"
  azs                           = ["us-west-2a", "us-west-2b", "us-west-2c"]
  additional_security_group_ids = "${module.eks.eks_worker_sg}"
  subnets                       = "${module.vpc_alpha_04172020.private_subnets}"
  ingress_cidr                  = "${module.vpc_alpha_04172020.vpc_cidr_block}"
  tags = {
    Terraform                                       = "true"
    Environment                                     = "${local.prefix_env}"
    "kubernetes.io/cluster/${local.prefix_env}-eks" = "owned"
  }
}

# EKS
module "eks" {
  source = "../../modules/eks_no_mgmt"

  cluster_name                    = "${local.prefix_env}-eks"
  cluster_version                 = "1.15"
  vpc_id                          = "${module.vpc_alpha_04172020.vpc_id}"
  private_subnets                 = ["${module.vpc_alpha_04172020.private_subnets}"]
  env                             = "${local.prefix_env}"
  key_name                        = "forte-dev-key"
  deploy_assume_role_arn          = "arn:aws:iam::984475896453:role/MissionEngineering"
  node_instance_type              = "m5.2xlarge"
  node_asg_max                    = "5"
  node_asg_min                    = "2"
  node_asg_desired                = "2"
  networking_node_instance_type   = "t2.large"
  additional_sg                   = "${module.security_groups.sg_eks_lb}"
  worker_additional_sg            = "${module.security_groups.sg_internal}"
  networking_node_asg_max         = "2"
  networking_node_asg_min         = "1"
  networking_node_asg_desired     = "1"
  networking_worker_additional_sg = "${module.security_groups.sg_internal}" # "${module.security_groups.sg_eks_lb}"]
  nway_node_instance_type         = "m5.2xlarge"
  nway_node_asg_max               = "4"
  nway_node_asg_min               = "1"
  nway_node_asg_desired           = "1"
  nway_worker_additional_sg       = "${module.security_groups.sg_internal}" # "${module.security_groups.sg_eks_lb}"]
  rally_node_instance_type        = "m5.2xlarge"
  rally_node_asg_max              = "4"
  rally_node_asg_min              = "1"
  rally_node_asg_desired          = "1"
  rally_worker_additional_sg      = "${module.security_groups.sg_internal}" # "${module.security_groups.sg_eks_lb}"]
  create_management_instance      = false
  mgmt_node_arn                   = "arn:aws:iam::984475896453:role/eks-mgmt"
}
