#!/usr/bin/env bash

# Use this tool for quick linting of your terraform plan file or to easily pull a terraform workspace's state down to your local machine. It
# is not intended for terraform apply, which should always be done in a gitlab pipeline.
#
# This script requires a file called rtft.conf to exist in the same directory as the script.  rtft.conf contains
# configuration items needed for the remote connection, but will not change for each run for a given project. It should be placed
# in your local {project_name}/tf/bin directory, but should not be checked into git. .gitignore has an entry for rtft.conf
# for this purpose.

# Example rtft.conf:

# tf_ssh_login={ssh login name}
# tf_project_name={project_name}  # must match the root directory name of your local project's directory
# tf_local_base_path="{base_path_before_project_name_directory" # (eg, /foo/bar) base path up to {project_name} directory (no trailing slash)
# read_only_vault_token={project_read_only_vault_token} # read only vault token created for specific project

# Ssh access must be configured using an ssh login and keypair.

# As a safety check, the script will rsync a copy of your local Git HEAD file and put it into GIT_HEAD on the remote. The remote
# script then checks to confirm that the tf workspace / env that you target with the --env param matches your checked out
# git branch.

# Command line params include the following:
# --env={tf_workspace}      : (required) terraform workspace to execute against; typically should match your local git HEAD branch
# --exec=[plan|pull]        : (required) terraform action to take on the target workspace / env using your local directory contents
# --debug=[true|false]      : (optional; defaults to false) turn on debugging
# --force=[true|false]      : (optional; defaults to false) to force the requested action when your local HEAD branch (ie, checked out branch) does not match the
#                               target env / workspace; this is usually not needed and should only be used if you know what you are doing

set -e; set +x

function rtftest
{
    ssh ${tf_ssh_login}@tf-bastion.internal.missioncloud.com "if [ ! -d ${tf_remote_project_path} ]; then mkdir -p ${tf_remote_project_path}; fi"
	rsync -avz --delete ${tf_local_project_path}/.git/HEAD ${tf_ssh_login}@tf-bastion.internal.missioncloud.com:${tf_remote_project_path}/GIT_HEAD
	rsync -avz --delete ${tf_local_project_path}/tf ${tf_ssh_login}@tf-bastion.internal.missioncloud.com:${tf_remote_project_path}

	# check if GIT_HEAD matches tf_env given by user
    ssh ${tf_ssh_login}@tf-bastion.internal.missioncloud.com <<EOF
    set -e

    if [ "${tf_debug}" == "true" ]; then set -x; fi
	echo \$(hostname)

	if [ -e ${tf_remote_project_path}/GIT_HEAD ]; then
	    head_cmd=\$(head -1 ${tf_remote_project_path}/GIT_HEAD)
	    branch_ref=\${head_cmd}
	    if [[ \${branch_ref} =~ ^.*/${tf_env}$ ]]; then
	        echo "\${branch_ref} matches ${tf_env}"
	    else
	        echo "\${branch_ref} does not match ${tf_env}"
	        echo "Checking if --force=true..."
	        if [ "${tf_force}" == "true" ]; then
	            echo "--force=true is set, ignoring GIT_HEAD to env mismatch."
	        else
	            echo "--force=true is not set, fix GIT_HEAD to env mismatch. Exiting now."
	            exit 1
	        fi
	    fi
	fi
EOF
    # run the deploy script
	ssh ${tf_ssh_login}@tf-bastion.internal.missioncloud.com <<EOF
	set -e

	# set VAULT_TOKEN env with individual vault token
    export VAULT_TOKEN="${read_only_vault_token}"

	if [ "${tf_debug}" == "true" ]; then set -x; fi
	cd ${tf_remote_project_path}
    echo \$(pwd)
    echo \$(ls -lah)

    if [[ "${tf_exec}" == "plan" ]]; then
        ./tf/bin/deploy.sh ${tf_env} ${tf_exec}
    elif [[ "${tf_exec}" == "pull" ]]; then
        cd "./tf/envs/${tf_env}"
        terraform --version
        terraform init
        terraform workspace select "${tf_env}"
        terraform state pull
    else
        echo "Terraform exec command ${tf_exec} not recognized.  Exiting."
    fi
EOF

}

function rtftest_conf
{
    echo "1: $1"
    local -n arr=$1

    echo "arr:"
    for i in "${arr[@]}"; do echo $i; done

    echo "dirname  \$0:"
    echo $(dirname $0)

    . $(dirname $0)/rtft.conf

    tf_local_project_path="${tf_local_base_path}/${tf_project_name}"
	tf_remote_base_path=\~/rtft
	tf_remote_project_path="${tf_remote_base_path}/${tf_project_name}"

    # parse cmd line

    # defaults
    tf_env=""
    tf_exec=""
    tf_force="false"
    tf_debug="false"
    for i in "${arr[@]}"; do
      case "$i" in
        --env=*)
          tf_env="${i#*=}"
          ;;
        --exec=*)
          tf_exec="${i#*=}"
          ;;
        --force=*)
          tf_force="${i#*=}"
          ;;
        --debug=*)
          tf_debug="${i#*=}"
          if [[ "$tf_debug" == "true" ]]; then set -x; fi
          ;;
        *)
          echo "Command line parameter ${i} not recognize. Exiting."
          exit 1
      esac
    done

    if [[ -z $tf_env || -z $tf_exec ]]; then echo "One or more required parameters are missing. Exiting."; exit 1; fi

}

g_arr=("$@")
rtftest_conf g_arr
rtftest
