#!/bin/bash

# This script takes 2 inputs, an environment, and a Terraform execution command (plan or apply).
# It is a wrapper script to be used in conjunction with a CI component and
# programmatically run the Terraform tools for managing infrastructure.


# Echo commands in the shell and exit on error
set -x
set -e

# Set variables
ENV="${1}"
EXEC="${2}"

# Navigate into environment directory
cd "./tf/envs/${ENV}"

# Terraform logic
terraform --version
terraform init
terraform workspace select "${ENV}" || (terraform workspace new "${ENV}" && terraform workspace select "${ENV}")
terraform init
terraform validate -input=false

if [[ "${EXEC}" == "plan" ]]
then
  terraform plan -input=false -out="${CI_COMMIT_SHA}"
fi

if [[ "${EXEC}" == "apply" ]]
then
  terraform apply -input=false "${CI_COMMIT_SHA}"
  rm -f "${CI_COMMIT_SHA}"
fi
